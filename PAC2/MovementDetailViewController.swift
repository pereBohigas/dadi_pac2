//
//  MovementDetailViewController.swift
//  PR2
//
//  Copyright © 2018 UOC. All rights reserved.
//

import UIKit

class MovementDetailViewController: UIViewController {
    
    @IBOutlet weak var rejectButton: UIButton!
    @IBOutlet weak var rejectedLabel: UILabel!
    
    // BEGIN-UOC-8.2
    
    @IBOutlet weak var movementAmountLabel: UILabel!
    @IBOutlet weak var movementDescriptionLabel: UILabel!
    @IBOutlet weak var movementDateLabel: UILabel!
    @IBOutlet weak var movementValueDateLabel: UILabel!
    @IBOutlet weak var movementAccountBalanceLabel: UILabel!
    
    var movement: Movement!
    var amountFormatter = NumberFormatter()
    var dateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFormatters()
        setupRejection()
        
        movementAmountLabel.text = amountFormatter.string(from: movement.amount as NSDecimalNumber)
        colorAmount(amountLabel: movementAmountLabel, amountQuantity: movement.amount)
        movementDescriptionLabel.text = movement.movementDescription
        
        movementDateLabel.text = dateFormatter.string(from: movement.date)
        movementValueDateLabel.text = dateFormatter.string(from: movement.valueDate)
        movementAccountBalanceLabel.text = amountFormatter.string(from: movement.balance as NSDecimalNumber)
    }
    
    func setupFormatters() {
        amountFormatter.usesGroupingSeparator = true
        amountFormatter.numberStyle = .currency
        // Uses currency formater for Catalonia, change if needed (the user's locale settings are available with "Locale.current")
        amountFormatter.locale = Locale(identifier: "ca_ES")
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
    }
    
    func colorAmount(amountLabel: UILabel, amountQuantity: Decimal) {
        if amountQuantity < 0 {
            amountLabel.textColor = UIColor.red
        } else {
            amountLabel.textColor = UIColor.black
        }
    }
    
    // END-UOC-8.2
    
    
    // BEGIN-UOC-9
    
    func setupRejection() {
        if movement.rejected {
            rejectButton.isHidden = true
            rejectedLabel.isHidden = false
        }
    }
    
    @IBAction func rejectMovement(_ sender: UIButton!) {
        movement.rejected = true
        
        if let navigationController = self.navigationController {
            navigationController.popViewController(animated: true)
        }
    }

    // END-UOC-9
}
