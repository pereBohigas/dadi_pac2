//
//  MovementsListViewController.swift
//  PR2
//
//  Copyright © 2018 UOC. All rights reserved.
//

import UIKit

class MovementsListViewController: UITableViewController {
    
    @IBOutlet weak var categoryControl: UISegmentedControl!
    
    // BEGIN-UOC-3
    
    var movements: [Movement]!
    var amountFormatter = NumberFormatter()
    var dateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
        setupFormatters()
        loadMovements()
    }
    
    func setupTable() {
        tableView.estimatedRowHeight = 75
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    // END-UOC-3
    
    
    // BEGIN-UOC-5 & BEGIN-UOC-6
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movements.count + 1
    }

    func setupFormatters() {
        amountFormatter.usesGroupingSeparator = true
        amountFormatter.numberStyle = .currency
        // Uses currency formater for Catalonia, change if needed (the user's locale settings are available with "Locale.current")
        amountFormatter.locale = Locale(identifier: "ca_ES")
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < movements.count {
            let movementCell = tableView.dequeueReusableCell(withIdentifier: "MovementCell", for: indexPath) as! MovementCell
            let singleMovement = movements[indexPath.row]
            
            movementCell.movementDescriptionLabel.text = singleMovement.movementDescription
            movementCell.movementAmountLabel.text = amountFormatter.string(from: singleMovement.amount as NSDecimalNumber)
            movementCell.movementDateLabel.text = dateFormatter.string(from: singleMovement.date)
            
            colorAmountIfNegative(amountLabel: movementCell.movementAmountLabel, amountQuantity: singleMovement.amount)
            colorCellIfRejected(rejectedStatus: singleMovement.rejected, cell: movementCell)
            return movementCell
        } else {
            let lastMovementCell = tableView.dequeueReusableCell(withIdentifier: "LastMovementCell", for: indexPath)
            return lastMovementCell
        }
    }
    
    func colorAmountIfNegative(amountLabel: UILabel, amountQuantity: Decimal) {
        if amountQuantity < 0 {
            amountLabel.textColor = UIColor.red
        } else {
            amountLabel.textColor = UIColor.black
        }
    }
    
    // END-UOC-5 & END-UOC-6
    
    
    // BEGIN-UOC-7
    
    @IBAction func categoryChanged(_ sender: UISegmentedControl) {
        loadMovements()
    }
    
    func loadMovements() {
        movements = Services.getMovements()
        
        switch categoryControl.selectedSegmentIndex {
        case 1:
            movements = movements.filter{ $0.category == "Transfers"}
        case 2:
            movements = movements.filter{ $0.category == "Credit cards"}
        case 3:
            movements = movements.filter{ $0.category == "Direct debits"}
        default:
            break
        }

        tableView.reloadData()
    }
    
    // END-UOC-7
    
    
    // BEGIN-UOC-8.1
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SegueToMovementDetail" {
            if let selectedRow = tableView.indexPathForSelectedRow?.row {
                let movementDetailScreen = segue.destination as! MovementDetailViewController
                movementDetailScreen.movement = movements[selectedRow]
            }
        }
    }
    
    // END-UOC-8.1
    
    
    // BEGIN-UOC-9
    
    func colorCellIfRejected(rejectedStatus: Bool, cell: MovementCell) {
        if rejectedStatus {
            cell.backgroundColor = UIColor.orange.lighter()
        } else {
            cell.backgroundColor = UIColor.white
        }
    }
    
    // END-UOC-9
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        tableView.reloadData()
    }
}
